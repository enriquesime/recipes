import { elements } from './base';

export const getInput = () => elements.getInput.value;
// export const searchForm = () => elements.searchForm;
// export const resultList = () => elements.resultList;

export const clearInput = () => {
    elements.getInput.value = '';
};

export const clearResultList = () => {
    elements.resultList.innerHTML= '';
};

const renderRecipe = recipe => {
    const markUp = `<li>
     <a class="results__link results__link--active" href="${recipe.recipe_id}">
        <figure class="results__fig">
            <img src="${recipe.image_url}" alt="${recipe.title}">
        </figure>
        <div class="results__data">
            <h4 class="results__name">${recipe.title}</h4>
            <p class="results__author">${recipe.publisher}</p>
        </div>
     </a>
    </li>`;
    elements.resultList.insertAdjacentHTML('beforeend',markUp);
}
export const renderResultList = recipes => {
    recipes.forEach(renderRecipe);
};

