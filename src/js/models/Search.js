import axios from "axios";

export default class Search {
    constructor(query) {
        this.query = query;
    }


    async  getResults() {
        const url = `https://forkify-api.herokuapp.com/api/search?&q=${this.query}`;
         debugger;
        try {
            const res = await axios(url);
            this.result = res.data.recipes;
            // console.log(recipes);
        } catch (error) {
            alert(error);
        }
    }

}