import Search from './models/Search';
import * as searchView from './views/searchView';



const state = {};

const controlSearch = async () =>{
    debugger;
   const query = searchView.getInput();
   console.log(query);
   if(query){
     //1 -get the query
    state.search = new Search(query);

    //2-prepare UI for recipes
    searchView.clearInput();
    searchView.clearResultList();
    //3- get results
    await state.search.getResults();

    //4-render results
    //console.log(state.search.result);
    searchView.renderResultList(state.search.result);

   }
}


document.querySelector('.search').addEventListener('submit', e =>{
    e.preventDefault();
    controlSearch();
});
